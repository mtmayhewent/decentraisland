App = {
  loading:false,
  contracts: {},


  load: async() => {
    //load app
    console.log("app loding...")

    await App.loadWeb3()
    await App.loadAccount()
    await App.loadContract()
    await App.render()

  },
  loadWeb3: async () => {
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider
      web3 = new Web3(web3.currentProvider)
    } else {
      window.alert("Please connect to Metamask.")
    }
    // Modern dapp browsers...
    if (window.ethereum) {
      window.web3 = new Web3(ethereum)
      try {
        // Request account access if needed
        await ethereum.enable()
        // Acccounts now exposed
        web3.eth.sendTransaction({/* ... */})
      } catch (error) {
        // User denied account access...
      }
    }
    // Legacy dapp browsers...
    else if (window.web3) {
      App.web3Provider = web3.currentProvider
      window.web3 = new Web3(web3.currentProvider)
      // Acccounts always exposed
      web3.eth.sendTransaction({/* ... */})
    }
    // Non-dapp browsers...
    else {
      console.log('Non-Ethereum browser detected. You should consider trying MetaMask!')
    }
  },

  loadAccount: async () => {
    // Set the current blockchain account
    App.account = web3.eth.accounts[0]


    console.log(App.account)


  },

  loadContract: async () =>{
    const decentraAto = await $.getJSON('decentra_ATO.json')

    App.contracts.decentraATO = TruffleContract(decentraAto)
    App.contracts.decentraATO.setProvider(App.web3Provider)
    App.decentraAto = await App.contracts.decentraATO.deployed()
    console.log(App.decentraAto.address)

   


    console.log(decentraAto)

    
  },

  render: async () => {
    if(App.loading){
      return
    }
    App.setLoading(true)
    $('#account').html("Connected to account # "+App.account)
    App.renderATO()
    

  },
  renderATO: async () =>{
    const delay = ms => new Promise(res => setTimeout(res, ms));
     var progressbar = $( "#progressbar" ),
      progressLabel = $( ".progress-label" );
 
    progressbar.progressbar({
      value: false,
      change: function() {
        progressLabel.text( progressbar.progressbar( "value" ) + "%" );
      },
      complete: function() {
        progressLabel.text( "Complete!" );
      }
    });
    const atoCount = await App.decentraAto.atoCount()
    const atoTemplate =  $('.atoTemplate')
    const Tasked_UnitsTemplate = $('.Tasked_Units')
    const Tasked_UnitTemplate = $('.Tasked_Unit')
    const AmsDataTemplate = $('.AmsData')
    const MSNACFTTemplate = $('.MSNACFT')
    const ARINFOTemplate = $('.ARINFO')
    const AMSNLOCTemplate = $('.AMSNLOC')
    const GTGTLOCTemplate = $('.GTGTLOC')
    const CONTROLATemplate = $('.CONTROLA')
    const ASUPTBYTemplate = $('.ASUPTBY')
    const PKGCMDTemplate = $('.PKGCMD')
    const REFTSKTemplate = $('.REFTSK')
    const ASUPTFORTemplate = $('.ASUPTFOR')
    const ASSACFTTemplate = $('.ASSIGNED_AIRCRAFT')
    const SUPPBYTemplate = $('.AirSupportedBy')
    const SUPPFORTemplate = $('.AirSupportFor')    
    const GROUNDTemplate = $('.Ground_Targets')
    const CONTROLASTemplate = $('.CONTROLAS')
    const Refueling_MissionsTemplate = $('.Refueling_Missions')
    const Refueling_Missions_containerTemplate = $('.Refueling_Missions_container')

    
    for (var i = 1; i <= atoCount; i++) {
      const ATO = await App.decentraAto.ATOs(i)
      
      const atoID       = ATO[0].toNumber()
      const atoExrName  = ATO[1].replace(/["]/g,"")
      const atoExerType = ATO[2].replace(/["]/g,"")
      const atoTskCNTRY = ATO[3].replace(/["]/g,"")
      const atoTskSVC   = ATO[4].replace(/["]/g,"")
      const atoFromTime = ATO[5].replace(/["]/g,"")
      const atoToTime   = ATO[6].replace(/["]/g,"")
      const taskedUnits = ATO[7].toNumber()
      const prevTaskedUnits = ATO[8].toNumber()
      //console.log(atoID)
      //console.log(atoExrName)
      //console.log(atoExerType)
      //console.log(atoTskCNTRY)
      //console.log(atoTskSVC)
      //console.log(atoFromTime)
      //console.log(atoToTime)
      //console.log(atoData)


      var $newAtoTemplate = atoTemplate.clone()
      var $newTasked_UnitsTemplate = Tasked_UnitsTemplate.clone()

      
      var $newMSNACFTTemplate = MSNACFTTemplate.clone()
      var $newARINFOTemplate = ARINFOTemplate.clone()
      var $newAMSNLOCTemplate = AMSNLOCTemplate.clone()
      var $newGTGTLOCTemplate = GTGTLOCTemplate.clone()
      var $newCONTROLATemplate = CONTROLATemplate.clone()
      var $newASUPTBYTemplate = ASUPTBYTemplate.clone()
      var $newASUPTFORTemplate = ASUPTFORTemplate.clone()
      $newAtoTemplate.find('#atoFromTime').html(atoFromTime)
      $newAtoTemplate.find('#atoToTime').html(atoToTime)
      
      $newAtoTemplate.find('#ATOName').html(atoExrName)
      $newAtoTemplate.find('#EXERType').html(atoExerType)
      $newAtoTemplate.find('#atoTskCNTRY').html(atoTskCNTRY)
      $newAtoTemplate.find('#atoTskSVC').html(atoTskSVC)
      $newAtoTemplate.find('#atoData').html("")
    
        
      for (var u = prevTaskedUnits+1; u<=taskedUnits+prevTaskedUnits; u++) {
        const Unparsedunit = await App.decentraAto.TaskedUnits(u)
        //console.log(Unparsedunit[0].toNumber())
        //console.log(Unparsedunit[1])
        //console.log(Unparsedunit[2])
        //console.log(Unparsedunit[3])
        //console.log(Unparsedunit[4].toNumber())
        //console.log(Unparsedunit[5].toNumber())
        const airMissionDataCount     = Unparsedunit[4].toNumber()
        const prevAirMissionDataCount = Unparsedunit[5].toNumber()
        var $newTasked_UnitTemplate = Tasked_UnitTemplate.clone()
       var $h3 = document.createElement('h3')
       $h3.append("Tasked Unit ID: "+Unparsedunit[1])
       $h3.append(" | Location:  "+Unparsedunit[2])
       $newTasked_UnitsTemplate.append($h3)
        for (var amsd = prevAirMissionDataCount+1; amsd<=airMissionDataCount+prevAirMissionDataCount; amsd++) {
              const UnparsedAirMissionData = await App.decentraAto.AirMissionDatas(amsd)
              //console.log(UnparsedAirMissionData[0].toNumber())
              //console.log(UnparsedAirMissionData[1])
              //console.log(UnparsedAirMissionData[2].toNumber())
              //console.log(UnparsedAirMissionData[3].toNumber())
              const missionCount      =   UnparsedAirMissionData[2].toNumber()
              const prevMissionCount  =   UnparsedAirMissionData[3].toNumber()
              const AmsData = JSON.parse(UnparsedAirMissionData[1])
              var $newAmsDataTemplate = AmsDataTemplate.clone()
              var $h3 = document.createElement('h3')
              $h3.append("Mission Number: "+AmsData.MISSION_NUMBER)
              $h3.append(" | Departure Location: "+AmsData.DEPARTURE_LOCATION+ " | Departure Time: "+AmsData.DEPARTURE_DATE_TIME)
              $h3.append(" | Arrival Location: "+AmsData.ARRIVAL_LOCATION+ " | Recovery Time: "+AmsData.RECOVERY_DATE_TIME)

              if(AmsData.PACKAGE_IDENTIFICATION != "-"){
                $h3.append(" | "+AmsData.PACKAGE_IDENTIFICATION )
              }
              if(AmsData.RESIDUAL_MISSION_INDICATOR != "N"){
                $h3.append(" | Residual Mission")
              }
              $newTasked_UnitTemplate.append($h3)
              var $newASSACFTTemplate = null
                var $newSUPPBYTemplate =null
                var $newSUPPFORTemplate = null
                var $newGROUNDTemplate = null
                var $newCONTROLASTemplate = null

            for (var mis = prevMissionCount+1; mis<=missionCount+prevMissionCount; mis++) {
            
            const UnparsedMission   = await App.decentraAto.AirMissionDataMessages(mis)
            //console.log(UnparsedMission[0].toNumber())
            //console.log(UnparsedMission[1])
            const mission1 = JSON.parse(UnparsedMission[1])
            switch(mission1.SET_ID){

                  case "MSNACFT":
                      $newASSACFTTemplate = ASSACFTTemplate.clone()
                      break;
                  case "ARINFO":

                      break;
                  case "AMSNLOC":

                      break;
                  case "GTGTLOC":
                      $newGROUNDTemplate = GROUNDTemplate.clone()      
                      break;
                  case "CONTROLA":
                       $newCONTROLASTemplate = CONTROLASTemplate.clone() 
                   
                      break;
                  case "ASUPTBY":
                      $newSUPPBYTemplate = SUPPBYTemplate.clone()
                      break;
                  case "ASUPTFOR":
                      $newSUPPFORTemplate = SUPPFORTemplate.clone()
                      break;
                  case "REFTSK":

                      break;
                  case "PKGCMD":
                      var $newPKGCMDTemplate = PKGCMDTemplate.clone()
                      //console.log(unit[ams][mission]);
                      var $amd3 = document.createElement('h3')
                      $amd3.append("Package Commander")
                      $newPKGCMDTemplate.append("Tasked Unit: "+mission1.TASKED_UNIT+"</br>")
                      $newPKGCMDTemplate.append("Aircraft CallSign: "+mission1.AIRCRAFT_CALL_SIGN+"</br>")
                      $newPKGCMDTemplate.append("Mission Number: "+mission1.MISSION_NUMBER+"</br>")
                      $newPKGCMDTemplate.append("Package ID: "+mission1.PACKAGE_IDENTIFICATION+"</br>")

                      $newAmsDataTemplate.append($amd3)
                      $newAmsDataTemplate.append($newPKGCMDTemplate)

                      break;

                  default:
                  //console.log(unit[ams][mission]);
                }

          }
              if($newASSACFTTemplate != null){
                var $ass = document.createElement('h3')
                $ass.append("Assigned Aircraft")
                $newAmsDataTemplate.append($ass)
               $newAmsDataTemplate.append($newASSACFTTemplate)
                $newASSACFTTemplate.show()
              }
              if($newSUPPBYTemplate != null){
                var $supb = document.createElement('h3')
                $supb.append("Air Support")
                $newAmsDataTemplate.append($supb)
               $newAmsDataTemplate.append($newSUPPBYTemplate)
                $newSUPPBYTemplate.show()
              }
              if($newSUPPFORTemplate != null){
                var $supf = document.createElement('h3')
                $supf.append("Air Support")
                $newAmsDataTemplate.append($supf)
               $newAmsDataTemplate.append($newSUPPFORTemplate)
                $newSUPPFORTemplate.show()
              }
              if($newGROUNDTemplate != null){
                var $grou = document.createElement('h3')
                $grou.append("Ground Tartgets")
                $newAmsDataTemplate.append($grou)
               $newAmsDataTemplate.append($newGROUNDTemplate)
                $newGROUNDTemplate.show()
              }
              if($newCONTROLASTemplate != null){
                var $grou = document.createElement('h3')
                $grou.append("Control Aircraft")
                $newAmsDataTemplate.append($grou)
               $newAmsDataTemplate.append($newCONTROLASTemplate)
                $newCONTROLASTemplate.show()
              }
              for (var mis2 = prevMissionCount+1; mis2<=missionCount+prevMissionCount; mis2++) {
            
            const UnparsedMission   = await App.decentraAto.AirMissionDataMessages(mis2)
            //console.log(UnparsedMission[0].toNumber())
            //console.log(UnparsedMission[1])
            const mission2 = JSON.parse(UnparsedMission[1])
            switch(mission2.SET_ID){

                    case "MSNACFT":
                      var $newMSNACFTTemplate = MSNACFTTemplate.clone()
                      //console.log(unit[ams][mission]);  
                      var $amd3 = document.createElement('h3')
                      $amd3.append(mission2.AIRCRAFT_CALL_SIGN)
                      $newMSNACFTTemplate.append("Number of Aircraft: "+mission2.NUMBER_OF_AIRCRAFT+"</br>")
                      $newMSNACFTTemplate.append("Aircraft Type: "+mission2.TYPE_OF_AIRCRAFT+"</br>")
                      $newMSNACFTTemplate.append("IFF/SIF Code: "+mission2.IFF_SIF_MODE_AND_CODE+"</br>")
                      $newMSNACFTTemplate.append("Primary Config Code: "+mission2.PRIMARY_CONFIGURATION_CODE+"</br>")
                      $newMSNACFTTemplate.append("Secondary Config Code: "+mission2.SECONDARY_CONFIGURATION_CODE+"</br>")
                      $newMSNACFTTemplate.append("Primary JU Address: "+mission2.PRIMARY_JU_ADDRESS+"</br>")
                      $newMSNACFTTemplate.append("TACAN Channel: "+mission2+"</br>")
                      if(mission2.ARINFO!=null){
                        var $newRefueling_Missions_containerTemplate = Refueling_Missions_containerTemplate.clone()
                        var $newRefueling_MissionsTemplate = Refueling_MissionsTemplate.clone()
                        var refuel = document.createElement('h3')
                        refuel.append("Refueling Missions")
                        $newRefueling_Missions_containerTemplate.append(refuel)
                        $newRefueling_Missions_containerTemplate.append($newRefueling_MissionsTemplate)
                        for(var refu in mission2.ARINFO){
                          var $newARINFOTemplate = ARINFOTemplate.clone()
                          //console.log(unit[ams][mission].ARINFO[refu]); 
                          var $amd2 = document.createElement('h3')
                          $amd2.append("Mission Number: "+mission2.ARINFO[refu].MISSION_NUMBER)
                          $newRefueling_MissionsTemplate.append($amd2)
                          $newARINFOTemplate.append("Aircraft CallSign: "+mission2.ARINFO[refu].AIRCRAFT_CALL_SIGN+"</br>")
                          $newARINFOTemplate.append("LINK16 CallSign: "+mission2.ARINFO[refu].LINK16_CALLSIGN+"</br>")
                          $newARINFOTemplate.append("Number of Aircraft in Cell: "+mission2.ARINFO[refu].NUMBER_OF_AIRCRAFT_IN_CELL+"</br>")
                          $newARINFOTemplate.append("Air Refueling Point: "+mission2.ARINFO[refu].AIR_REFUELING_CONTROL_POINT+"</br>")
                          $newARINFOTemplate.append("Air Refueling Date and Time: "+mission2.ARINFO[refu].AIR_REFUELING_CONTROL_DAY_TIME+"</br>")
                          $newARINFOTemplate.append("Refueling Altitude: "+mission2.ARINFO[refu].ALTITUDE_IN_HUNDREDS_OF_FEET+"</br>")
                          $newARINFOTemplate.append("Beacon: "+mission2.ARINFO[refu].BEACON+"</br>")
                          $newARINFOTemplate.append("Cell Sequance Number: "+mission2.ARINFO[refu].CELL_SQUENCE_NUMBER+"</br>")
                          $newARINFOTemplate.append("End Date and Time of Air Refueling: "+mission2.ARINFO[refu].DY_TMANDMON_OF_END_AR+"</br>")
                          $newARINFOTemplate.append("IFF/SIF Code: "+mission2.ARINFO[refu].IFF_SIF_MODE_AND_CODE+"</br>")
                          $newARINFOTemplate.append("Fuel Offload: "+mission2.ARINFO[refu].OFFLOAD_FUEL+"</br>")
                          $newARINFOTemplate.append("Refueling Aircraft Type: "+mission2.ARINFO[refu].REFUELING_AIRCRAFT_TYPE+"</br>")
                          $newARINFOTemplate.append("Primary Frequency: "+mission2.ARINFO[refu].PRIMARY_FREQUENCY+"</br>")
                          $newARINFOTemplate.append("Secondary Frequency: "+mission2.ARINFO[refu].SECONDARY_FREQUENCY+"</br>")
                          $newARINFOTemplate.append("TACAN Channel: "+mission2.ARINFO[refu].TACAN_CHANNEL+"</br>")
                          $newRefueling_MissionsTemplate.append($newARINFOTemplate)
                          $newRefueling_MissionsTemplate.show()


                        } 
                        $newMSNACFTTemplate.append($newRefueling_Missions_containerTemplate)
                        $newRefueling_Missions_containerTemplate.show()
                        

                      }

                      $newASSACFTTemplate.find(".Aircraft").append($amd3)
                      $newASSACFTTemplate.find(".Aircraft").append($newMSNACFTTemplate)
                      break;
                  case "AMSNLOC":
                      var $newAMSNLOCTemplate = AMSNLOCTemplate.clone()
                      //console.log(unit[ams][mission]);
                      var $amd3 = document.createElement('h3')
                      $amd3.append("Air Mission Location: "+mission2.MISSION_LOCATION_NAME)                      
                      $newAMSNLOCTemplate.append("Priority: "+mission2.MISSION_PRIORITY+"</br>")
                      $newAMSNLOCTemplate.append("Target/Facility Name: "+mission2.MISSION_LOCATION_NAME+"</br>")
                      $newAMSNLOCTemplate.append("Mission Start Time: "+mission2.DAY_TIME_AND_MONTH_OF_START+"</br>")
                      $newAMSNLOCTemplate.append("Mission End Time: "+mission2.DAY_TIME_AND_MONTH_OF_STOP+"</br>")
                      $newAMSNLOCTemplate.append("Mission Altitude: "+mission2.ALTITUDE_IN_HUNDREDS_OF_FEET+"00 FT</br>")

                      $newAmsDataTemplate.append($amd3)
                      $newAmsDataTemplate.append($newAMSNLOCTemplate)

                      break;
                  case "GTGTLOC":
                      var $newGTGTLOCTemplate = GTGTLOCTemplate.clone()
                      //console.log(unit[ams][mission]);
                      var $amd3 = document.createElement('h3');
                      $amd3.append("Target ID: "+mission2.COMPONENT_TARGET_ID);
                      if(mission2.PRIMARY_ALTERNATE_DESIGNATOR == "P"){
                        $amd3.append(" | Primary")
                      }
                      else{
                        $amd3.append(" | Alternate")
                      }
                      $newGTGTLOCTemplate.append("Priority: "+mission2.PRIORITY+"</br>")
                      $newGTGTLOCTemplate.append("Target/Facility Name: "+mission2.TARGET_FACILITY_NAME+"</br>")
                      $newGTGTLOCTemplate.append("Target Type: "+mission2.TARGET_TYPE+"</br>")
                      $newGTGTLOCTemplate.append("Target Identifier: "+mission2.TARGET_IDENTIFIER+"</br>")
                      $newGTGTLOCTemplate.append("Target Objective: "+mission2.TARGET_OBJECTIVE+"</br>")
                      $newGTGTLOCTemplate.append("LINK16 Track Number Reference: "+mission2.LINK_16_TRACK_NUMBER_REFERENCE_FOR_PRE_PLANNED_TARGET+"</br>")
                      $newGTGTLOCTemplate.append("Time on Target: "+mission2.TIME_ON_TARGET+"</br>")
                      $newGTGTLOCTemplate.append("Desired mean point of impact (DMPI): "+mission2.DESIRED_MEAN_POINT_OF_IMPACT+"</br>")
                      $newGTGTLOCTemplate.append("DMPI Description: "+mission2.DMPI_DESCRIPTION+"</br>")
                      $newGTGTLOCTemplate.append("DMPI elevation: "+mission2.DMPI_ELEVATION_IN_FT_OR_METERS+"</br>")
                      $newGTGTLOCTemplate.append("No Earlier Than Time: "+mission2.NOT_EARLIER_THAN_DAY_TIME_AND_MONTH+"</br>")
                      $newGTGTLOCTemplate.append("No Later Than Time: "+mission2.NLT_TIME+"</br>")
                      $newGTGTLOCTemplate.append("Geodetic Datum: "+mission2.GEODETIC_DATUM+"</br>")
                      $newGTGTLOCTemplate.append("Comments: "+mission2.GROUND_TARGET_COMMENTS+"</br>")


                      $newGROUNDTemplate.find(".Locations").append($amd3);
                      $newGROUNDTemplate.find(".Locations").append($newGTGTLOCTemplate);
                      break;
                  case "CONTROLA":
                      var $newCONTROLATemplate = CONTROLATemplate.clone()
                      //console.log(unit[ams][mission]);
                      var $amd3 = document.createElement('h3')
                      $amd3.append(mission2.CALL_SIGN)
                      $newCONTROLATemplate.append("Type of Aircraft: "+mission2.TYPE_AIRCRAFT_CONTROL_AGENCY+"</br>")
                      $newCONTROLATemplate.append("Link 16 CallSign: "+mission2.LINK16_CALLSIGN+"</br>")
                      $newCONTROLATemplate.append("Report in Point: "+mission2.REPORT_IN_POINT+"</br>")
                      $newCONTROLATemplate.append("Primary Frequency: "+mission2.PRIMARY_FREQ+"</br>")
                      $newCONTROLATemplate.append("Secondary Frequency: "+mission2.SECONDARY_FREQ+"</br>")
                      


                      $newCONTROLASTemplate.find(".Aircraft").append($amd3)
                      $newCONTROLASTemplate.find(".Aircraft").append($newCONTROLATemplate)
                      break;
                  case "ASUPTBY":
                      var $newASUPTBYTemplate = ASUPTBYTemplate.clone()
                      var $amd3 = document.createElement('h3')
                      $amd3.append("Support Unit ID: "+ mission2.TASKED_UNIT)
                      $newASUPTBYTemplate.append("Number of Aircraft: "+mission2.NUMBER_OF_AIRCRAFT+"</br>")
                      $newASUPTBYTemplate.append("Aircraft CallSign: "+mission2.AIRCRAFT_CALLSIGN+"</br>")
                      $newASUPTBYTemplate.append("LINK16 CallSign: "+mission2.LINK16_CALLSIGN+"</br>")
                      $newASUPTBYTemplate.append("Aircraft Type: "+mission2.AIRCRATFT_TYPE+"</br>")
                      $newASUPTBYTemplate.append("Mission Number: "+mission2.MISSION_NUMBER+"</br>")
                      $newASUPTBYTemplate.append("Primary Mission Type: "+mission2.PRIMARY_MISSION_TYPE+"</br>")
                      $newASUPTBYTemplate.append("Primary Frequency: "+mission2.PRIMARY_FREQ+"</br>")
                      $newASUPTBYTemplate.append("Secondary Frequency: "+mission2.SECONDARY_FREQ+"</br>")
                      $newSUPPBYTemplate.find(".Aircraft").append($amd3)
                      $newSUPPBYTemplate.find(".Aircraft").append($newASUPTBYTemplate)
                      //console.log(unit[ams][mission]);
                      break;
                  case "ASUPTFOR":
                      var $newASUPTFORTemplate = ASUPTFORTemplate.clone()
                      //console.log(unit[ams][mission]);
                      var $amd3 = document.createElement('h3')
                      $amd3.append("Supporting Unit ID: "+ mission2.TASKED_UNIT)
                      $newASUPTFORTemplate.append("Number of Aircraft: "+mission2.NUMBER_OF_AIRCRAFT+"</br>")
                      $newASUPTFORTemplate.append("Aircraft CallSign: "+mission2.AIRCRAFT_CALLSIGN+"</br>")
                      $newASUPTFORTemplate.append("LINK16 CallSign: "+mission2.LINK16_CALLSIGN+"</br>")
                      $newASUPTFORTemplate.append("Aircraft Type: "+mission2.AIRCRATFT_TYPE+"</br>")
                      $newASUPTFORTemplate.append("Mission Number: "+mission2.MISSION_NUMBER+"</br>")
                      $newASUPTFORTemplate.append("Primary Mission Type: "+mission2.PRIMARY_MISSION_TYPE+"</br>")
                      $newASUPTFORTemplate.append("Primary Frequency: "+mission2.PRIMARY_FREQ+"</br>")
                      $newASUPTFORTemplate.append("Secondary Frequency: "+mission2.SECONDARY_FREQ+"</br>")
                      

                      $newSUPPFORTemplate.find(".Aircraft").append($amd3)
                      $newSUPPFORTemplate.find(".Aircraft").append($newASUPTFORTemplate)
                      break;
                  case "REFTSK":
                      console.log(mission2);
                      var $newREFTSKTemplate = REFTSKTemplate.clone()
                      var $amd3 = document.createElement('h3')
                      $amd3.append("Refueling Task")
                      $newREFTSKTemplate.append("Air Refueling System: "+mission2.AERIAL_REFUELING_SYSTEM+"</br>")
                      $newREFTSKTemplate.append("Primary Frequency: "+mission2.PRIMARY_FREQUENCY+"</br>")
                      $newREFTSKTemplate.append("Secondary Frequency: "+mission2.SECONDARY_FREQUENCY+"</br>")
                      $newREFTSKTemplate.append("TACAN Channel: "+mission2.TACAN_CHANNEL+"</br>")
                      $newREFTSKTemplate.append("Reciver and Tanker: "+mission2.RECEIVER_AND_TANKER+"</br>")
                      $newREFTSKTemplate.append("Total Offload Fuel: "+mission2.TOTAL_OFFLOAD_FUEL+"</br>")
                      $newREFTSKTemplate.append("Alert Contingency Offload Fuel: "+mission2.ALERT_CONTINGENCY_OFFLOAD_FUEL+"</br>")
                      $newREFTSKTemplate.append("Beacon: "+mission2.BEACON+"</br>")


                                  


                      $newAmsDataTemplate.append($amd3)
                      $newAmsDataTemplate.append($newREFTSKTemplate)
                      break;
                  case "PKGCMD":
                      

                      break;

                  default:
                  console.log(mission2);

                }
                
 
                     

          }


          $newTasked_UnitTemplate.append($newAmsDataTemplate)
        }

        
       
       //console.log($h3)
       

          //console.log(unit.TASKED_UNIT_DESIGNATOR);
          
          $newTasked_UnitsTemplate.append($newTasked_UnitTemplate)
       }

        $newAtoTemplate.find('#atoData').append($newTasked_UnitsTemplate)
      


      $('#atoList').append($newAtoTemplate)

      let li = document.createElement('li')
      let link = document.createElement('a')
      link.href = "#tabs-"+atoID
      link.append(atoExrName)
      
      li.append(link)

      $('#ATOs').append(li)



      $newAtoTemplate.hide(function(){

        this.id = "tabs-"+atoID;
      })


      var val = 100 / atoCount * i

 
                  progressbar.progressbar( "value", val );
    await delay(3000);

    
      }
      $(".Tasked_Units").accordion({
        active:false,
        collapsible: true,
        heightStyle: "content"
      });
      $(".Tasked_Unit").accordion({
        active:false,
        collapsible: true,
        heightStyle: "content"
      });
      $(".AmsData").accordion({
        active:false,
        collapsible: true,
        heightStyle: "content"
      });
      $(".Aircraft").accordion({
        active:false,
        collapsible: true,
        heightStyle: "content"
      });
      $(".Locations").accordion({
        active:false,
        collapsible: true,
        heightStyle: "content"
      });
      $(".Refueling_Missions").accordion({
        active:false,
        collapsible: true,
        heightStyle: "content"
      });
      $(".Refueling_Missions_container").accordion({
        active:false,
        collapsible: true,
        heightStyle: "content"
      });
      


      $( "#atoList" ).tabs({
         collapsible: true
       });
      $( "#atoList" ).show()
      App.setLoading(false)

  },

  addATO: async() =>{

    const UnparsedATO = $('#ATOData').val()

    const ATOData = JSON.parse(UnparsedATO)
    console.log(ATOData)

    console.log(ATOData.TASKEDUNITS)
    
    for(unit in ATOData.TASKEDUNITS){
      console.log(ATOData.TASKEDUNITS[unit].TASKED_UNIT_DESIGNATOR)
      const Unit = ATOData.TASKEDUNITS[unit]
      console.log(Unit)
      for (var ams = 0 ; ams < Unit.AMSNDAT.length; ams += 2){
        console.log(Unit.AMSNDAT[ams])
        const missions=Unit.AMSNDAT[ams+1]
        for(mission in missions){
          await App.decentraAto.addAirMissionDataMessage(JSON.stringify(missions[mission]))
          console.log(JSON.stringify(missions[mission]))

        }
        await App.decentraAto.addAirMissionData(JSON.stringify(Unit.AMSNDAT[ams])) 
        
        //console.log(Unit.AMSNDAT[ams+1])

      }
      await App.decentraAto.addTaskedUnit(Unit.TASKED_UNIT_DESIGNATOR, Unit.TASKED_UNIT_LOCATION, Unit.COMMENTS)

    }
    await App.decentraAto.loadNewATO(ATOData.EXER.EXERCISE_NICKNAME, ATOData.MSGID.MESSAGE_TEXT_FORMAT_IDENTIFIER, ATOData.TSKCNTRY, ATOData.TASKED_SERVICE, ATOData.TIMEFRAM.ENDING_DATE_TIME_GROUP, ATOData.TIMEFRAM.BEGINNING_DATE_TIME_GROUP)
   
    document.location.reload(true)

   // await  App.decentraAto.addAirMissionDataMessage(messageData)
    

  },

  setLoading: (boolean)=>{
    App.loading = boolean
    const loader = $('#loader')
    const content = $('#content')

    if(boolean){
      loader.show()
      content.hide()
    }else{
      loader.hide()
      content.show()
    }
  }

  
}

$(() =>{
  $(window).load(() =>{
    App.load()
  })
})
