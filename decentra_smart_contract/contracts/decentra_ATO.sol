pragma solidity ^0.5.0;

/**
 * The decentra_ATO contract does this and that...
 */
contract decentra_ATO {
	uint public atoCount = 0;
	uint public taskedUnitCount = 0;
	uint public prevTaskedUnitCount = 0;
	uint public airMissionDataMessageCount = 0;
	uint public prevAirMissionDataMessageCount = 0;
	uint public airMissionDataCount = 0;
	uint public prevAirMissionDataCount = 0;


	struct ATO {
		uint id;
		string exerName;
		string exerType;
		string tskCNTRY;
		string tskSVC;
		string fromTime;
		string toTime;
		uint taskedUnits;
		uint prevTaskedUnit;
	}

	struct TaskedUnit{
		uint id;
		string unitCallsign;
		string unitLocation;
		string comments;
		uint airMissionData;
		uint prevAirMissionData;
	}

	struct AirMissionDataMessage{
		uint id;
		string messageData;
	}
	
	struct AirMissionData{
		uint id;
		string AMSData;
		uint airMissionDataMessages;
		uint prevAirMissionDataMessage;
		
	}



	mapping(uint => ATO) public ATOs;
	mapping(uint => TaskedUnit) public TaskedUnits;
	mapping(uint => AirMissionData) public AirMissionDatas;	
	mapping(uint => AirMissionDataMessage) public AirMissionDataMessages;

	event ATOLoaded(
		uint id,
		string exerName,
		string exerType,
		string tskCNTRY,
		string tskSVC,
		string fromTime,
		string toTime,
		uint taskedUnits,
		uint prevTaskedUnit
		);


	event TaskedUnitAdded(
		uint id,
		string unitCallsign,
		string unitLocation,
		string comments,
		uint airMissionData,
		uint prevAirMissionData
	);

	event AirMissionDataMessageAdded(
		uint id,
		string messageData
	);
	
	event AirMissionDataAdded(
		uint id,
		string AMSData,
		uint airMissionDataMessages,
		uint prevAirMissionDataMessage
		
	);





	function loadNewATO(string memory _exerName, string memory _exerType, string memory _tskCNTRY, string memory _tskSVC, string memory _toTime,
		 string memory _fromTime) public{
		atoCount ++;
		ATOs[atoCount] = ATO(atoCount, _exerName, _exerType, _tskCNTRY, _tskSVC, _fromTime, _toTime, taskedUnitCount , prevTaskedUnitCount);
		prevTaskedUnitCount += taskedUnitCount;
		taskedUnitCount = 0;
		emit ATOLoaded(atoCount, _exerName, _exerType, _tskCNTRY, _tskSVC, _fromTime, _toTime, taskedUnitCount , prevTaskedUnitCount);

		
	}

	function addTaskedUnit(string memory _unitCallsign, string memory _unitLocation, string memory _comments) public{
		taskedUnitCount++;
		uint currentTaskedUnit = taskedUnitCount+prevTaskedUnitCount;
		TaskedUnits[currentTaskedUnit] = TaskedUnit(currentTaskedUnit, _unitCallsign, _unitLocation, _comments, airMissionDataCount,prevAirMissionDataCount );
		prevAirMissionDataCount += airMissionDataCount;
		airMissionDataCount = 0 ;

	emit TaskedUnitAdded(currentTaskedUnit, _unitCallsign, _unitLocation, _comments, airMissionDataCount,prevAirMissionDataCount );

	}

	function addAirMissionDataMessage(string memory _messageData) public{
		airMissionDataMessageCount++;
		uint currentAirMissionDataMessage = airMissionDataMessageCount+prevAirMissionDataMessageCount;
		AirMissionDataMessages[currentAirMissionDataMessage] = AirMissionDataMessage(currentAirMissionDataMessage, _messageData);
		emit AirMissionDataMessageAdded(currentAirMissionDataMessage, _messageData);
					

	}
	function addAirMissionData(string memory _AMSData) public{
		airMissionDataCount++;
		uint currentAirMissionData = airMissionDataCount+prevAirMissionDataCount;
		AirMissionDatas[currentAirMissionData] = AirMissionData(currentAirMissionData, _AMSData, airMissionDataMessageCount, prevAirMissionDataMessageCount );
		prevAirMissionDataMessageCount += airMissionDataMessageCount;
		airMissionDataMessageCount = 0 ;
		emit AirMissionDataAdded(currentAirMissionData, _AMSData, airMissionDataMessageCount, prevAirMissionDataMessageCount );
	}

	




}

