module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8042,
      network_id: "42",
      gas: 4712388
    }
  },
  solc: {
    optimizer: {
      enabled: true,
      runs: 200
    }
  }
}
